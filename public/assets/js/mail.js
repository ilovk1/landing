
$(document).ready(function () {
    $('#contact-us-btn').click(function (e) {
        return sendEmail(e);
    })
})
function sendEmail(e) {
    e.preventDefault();
    $('#email-sent-alert').addClass('collapse');
    $('#email-not-sent-alert').addClass('collapse');

    if ($('#contact')[0].reportValidity()) {
        let body = `Name: ${$('#name').val()} <br/>
                    Phone: ${$('#phone').val()} <br/>
                    Email: ${$('#email').val()}} <br/>
                    Subject: ${$('#subject').val()} <br/>
                    Message: ${$('#message').val()}`

        Email.send({
            SecureToken: "1319d045-b16b-4d2a-b9ec-db9b5b951654",
            To: 'contact@ilovk.com',
            From: "svolkovichs@gmail.com",
            Subject: `ilovk website ${$('#subject').val()}`,
            Body: body,
        }).then(
            message => {
                if (message === 'OK') {
                    $('#email-sent-alert').removeClass('collapse');
                    $('#name').val('');
                    $('#phone').val('');
                    $('#email').val('');
                    $('#subject').val('');
                    $('#message').val('');
                } else {
                    $('#email-not-sent-alert').removeClass('collapse');
                }
            }
        );
    }

    return false
}